<div class="mt-6 sm:mt-5 space-y-6">
    <x-input.group label="Title" for="title" :error="$errors->first('title')">
        <x-input.text name="title" id="title" value="{{ isset($vulnerability) ? $vulnerability->title : old('title') }}" />
    </x-input.group>
    <x-input.group label="Overview" for="overview" :error="$errors->first('overview')" help-text="Write a overview on a vulnerability.">
        <x-input.rich-text name="overview" id="overview" value="{{ isset($vulnerability) ? $vulnerability->overview : old('overview') }}" />
    </x-input.group>
</div>
<x-input.group label="Description" for="description" :error="$errors->first('description')" help-text="Write a vulnerability description">
    <x-input.rich-text name="description" id="description" value="{{ isset($vulnerability) ? $vulnerability->description : old('description') }}" />
</x-input.group>

<x-input.group label="How To Prevent" for="how_to_prevent" :error="$errors->first('how_to_prevent')" help-text="Write a how to prevent a vulnerability">
    <x-input.rich-text name="how_to_prevent" id="how_to_prevent" value="{{ isset($vulnerability) ? $vulnerability->how_to_prevent : old('how_to_prevent') }}" />
</x-input.group>

<div class="flex justify-end">
    <span class="inline-flex rounded-md shadow-sm">
        <a href="{{ route('vulnerabilities.index') }}" class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
            {{ __('Cancel') }}
        </a>
    </span>

    <span class="ml-3 inline-flex rounded-md shadow-sm">
        <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
            {{ __('Save') }}
        </button>
    </span>
</div>
