<x-app-layouts>
    <x-slot name="title">{{ __('Create New Vulnerability') }}</x-slot>
    <form action="{{ route('vulnerabilities.store') }}" method="POST">
        @csrf
        @include('vulnerabilities._includes.fields')
    </form>
</x-app-layouts>


