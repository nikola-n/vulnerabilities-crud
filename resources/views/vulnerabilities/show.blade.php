<x-app-layouts>
    <div class="py-6">
        <x-slot name="title">
            <a href="{{ route('vulnerabilities.index') }}" class="hover:text-indigo-600">
                {{ __('Vulnerabilities') }}
            </a>
            /
            {{ $vulnerability->title }}
        </x-slot>
    </div>
    <div class="relative bg-white">
        <div class="lg:absolute lg:inset-0">
            <div class="lg:absolute lg:inset-y-0 lg:left-0 lg:w-1/2">
                <img class="h-56 w-full object-cover rounded shadow-2xl lg:absolute lg:h-full" src="https://images.unsplash.com/photo-1522071820081-009f0129c71c?ixlib=rb-1.2.1&auto=format&fit=crop&w=1567&q=80" alt="">
            </div>
        </div>
        <div class="relative pt-12 pb-16 px-4 sm:pt-16 sm:px-6 lg:px-8 lg:max-w-7xl lg:mx-auto lg:grid lg:grid-cols-2">
            <div class="lg:col-start-2 lg:pl-8">
                <div class="text-base max-w-prose mx-auto lg:max-w-lg lg:ml-auto lg:mr-0">
                    <h2 class="leading-6 text-indigo-600 font-semibold tracking-wide uppercase">{{ __('Vulnerability Category') }}</h2>
                    <h3 class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">{{ $vulnerability->title }}</h3>
                    <div class="mt-5 prose prose-indigo text-gray-500">
                        <h3>{{ __('Overview') }}</h3>
                        <p class="mt-8 text-lg text-gray-500">{!! $vulnerability->overview !!} </p>
                    </div>
                    <div class="mt-5 prose prose-indigo text-gray-500">
                        <h3>{{ __('Description') }}</h3>
                        {!! $vulnerability->description !!}
                    </div>
                    <div class="mt-5 prose prose-indigo text-gray-500">
                        <h3>{{ __('How To Prevent') }}</h3>
                        <p>{!! $vulnerability->how_to_prevent !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layouts>
