<x-app-layouts>
    <x-slot name="title">{{ __("Edit Vulnerability - $vulnerability->title") }}</x-slot>
    <form action="{{ route('vulnerabilities.update', $vulnerability) }}" method="POST">
        @csrf
        @method('PUT')
        @include('vulnerabilities._includes.fields')
    </form>
</x-app-layouts>
