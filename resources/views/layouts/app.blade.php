<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vulnerabilities CRUD</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/@tailwindcss/typography@0.4.x/dist/typography.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/@tailwindcss/custom-forms@0.2.1/dist/custom-forms.min.css" />
    <script defer src="https://unpkg.com/alpinejs@3.5.1/dist/cdn.min.js"></script>
    @stack('styles')
    <style>
        [x-cloack] {
            display: none;
        }
    </style>
</head>
<body>
<div>
    <!-- Static sidebar for mobile -->
    @include('layouts._includes.sidebar-mobile')

    <!-- Static sidebar for desktop -->
    @include('layouts._includes.sidebar-desktop')

    <div class="md:pl-64 flex flex-col flex-1">
        @if(session()->get('message'))
            <x-alert message="{{ session('message') }}" />
        @endif
        <main>
            <div class="py-6">
                <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                    <h1 class="text-2xl font-semibold text-gray-900">{{ $title }}</h1>
                </div>
                <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                    {{ $slot }}
                </div>
            </div>
        </main>
    </div>
</div>
@stack('scripts')
</body>
</html>
