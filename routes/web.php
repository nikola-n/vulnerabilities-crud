<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\VulnerabilityController;

Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');
Route::resource('vulnerabilities', VulnerabilityController::class);
