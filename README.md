# Simple CRUD Actions

- git clone https://gitlab.com/nikola-n/vulnerabilities-crud.git
- cp env.example .env
- composer install
- npm install && npm run dev
- php artisan key:generate
- php artisan migrate --seed

`Enjoy testing! Also there are two phpunit tests
run: vendor/bin/phpunit`
