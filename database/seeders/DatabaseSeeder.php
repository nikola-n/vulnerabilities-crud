<?php

namespace Database\Seeders;

use App\Models\Vulnerability;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Vulnerability::create([
            'title'          => 'A01:2021 – Broken Access Control',
            'overview'       => '<p>Moving up from the fifth position, 94% of applications were tested for some form of broken access control with the average incidence rate of 3.81%, and has the most occurrences in the contributed dataset with over 318k. Notable Common Weakness Enumerations (CWEs) included are CWE-200: Exposure of Sensitive Information to an Unauthorized Actor, CWE-201: Exposure of Sensitive Information Through Sent Data, and CWE-352: Cross-Site Request Forgery.</p>',
            'description'    => "<p>Access control enforces policy such that users cannot act outside of their intended permissions. Failures typically lead to unauthorized information disclosure, modification, or destruction of all data or performing a business function outside the user's limits. Common access control vulnerabilities include:</p>
<ul>
<li>
Violation of the principle of least privilege or deny by default, where access should only be granted for particular capabilities, roles, or users, but is available to anyone.
</li>
<li>
Bypassing access control checks by modifying the URL (parameter tampering or force browsing), internal application state, or the HTML page, or by using an attack tool modifying API requests.
</li>
<li>
Permitting viewing or editing someone else's account, by providing its unique identifier (insecure direct object references)
</li>
<li>
Accessing API with missing access controls for POST, PUT and DELETE.
</li>
</ul>
",
            'how_to_prevent' => "<p>Access control is only effective in trusted server-side code or server-less API, where the attacker cannot modify the access control check or metadata.</p>
<ul>
<li>
Model access controls should enforce record ownership rather than accepting that the user can create, read, update, or delete any record.
</li>
<li>
Implement access control mechanisms once and re-use them throughout the application, including minimizing Cross-Origin Resource Sharing (CORS) usage.
</li>
<li>
Model access controls should enforce record ownership rather than accepting that the user can create, read, update, or delete any record.
</li>
</ul>
",
        ]);

        Vulnerability::create([
            'title'          => 'A02:2021 – Cryptographic Failures',
            'overview'       => '<p>Shifting up one position to #2, previously known as Sensitive Data Exposure, which is more of a broad symptom rather than a root cause, the focus is on failures related to cryptography (or lack thereof). Which often lead to exposure of sensitive data. Notable Common Weakness Enumerations (CWEs) included are CWE-259: Use of Hard-coded Password, CWE-327: Broken or Risky Crypto Algorithm, and CWE-331 Insufficient Entropy .</p>',
            'description'    => "<p>The first thing is to determine the protection needs of data in transit and at rest. For example, passwords, credit card numbers, health records, personal information, and business secrets require extra protection, mainly if that data falls under privacy laws, e.g., EU's General Data Protection Regulation (GDPR), or regulations, e.g., financial data protection such as PCI Data Security Standard (PCI DSS). For all such data:</p>
<ul>
<li>
Is any data transmitted in clear text? This concerns protocols such as HTTP, SMTP, FTP also using TLS upgrades like STARTTLS. External internet traffic is hazardous. Verify all internal traffic, e.g., between load balancers, web servers, or back-end systems.
</li>
<li>
Are default crypto keys in use, weak crypto keys generated or re-used, or is proper key management or rotation missing? Are crypto keys checked into source code repositories?
</li>
<li>
Are initialization vectors ignored, reused, or not generated sufficiently secure for the cryptographic mode of operation? Is an insecure mode of operation such as ECB in use? Is encryption used when authenticated encryption is more appropriate?
</li>
<li>
Is encryption not enforced, e.g., are any HTTP headers (browser) security directives or headers missing?
</li>
</ul>
",
            'how_to_prevent' => "<p>Do the following, at a minimum, and consult the references:</p>
<ul>
<li>
Classify data processed, stored, or transmitted by an application. Identify which data is sensitive according to privacy laws, regulatory requirements, or business needs.
</li>
<li>
Don't store sensitive data unnecessarily. Discard it as soon as possible or use PCI DSS compliant tokenization or even truncation. Data that is not retained cannot be stolen.
</li>
<li>
Ensure up-to-date and strong standard algorithms, protocols, and keys are in place; use proper key management.
</li>
</ul>
",
        ]);

        Vulnerability::factory(10)->create();
    }

}
